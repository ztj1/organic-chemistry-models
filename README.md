# Organic Chemistry Models

COM S 574 final project by Zach Johnson. Analysis and machine learning models of organic chemistry data, including support vector regressor, neural network, and random forest models.